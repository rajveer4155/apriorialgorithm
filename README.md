# README #

#### Apriori Algorithm (Data Mining) in Java ####

### Summary 
1. This source code is the implementation of Data mining's most famous Algorithm, Apriori Algorithm in Java
2. It works on the sample datasets already present in Project Repo.
3. Each dataset consists of 20 Transactions.
4. It takes input as minimum Support and Confidence required in percentage and generates Association rules.